from unittest import TestCase, mock

from satlib.tasks import Task, TaskManager


class TestTasks(TestCase):

    def test_task_frequency(self):
        callback = mock.Mock()
        task = Task(callback=callback, frequency=30)
        self.assertEqual(task.callback, callback)
        self.assertEqual(task.frequency, 30)
        self.assertEqual(task.interval, 1/30)
        self.assertEqual(task.last_called, 0.00)

    def test_task_interval(self):
        callback = mock.Mock()
        task = Task(callback=callback, interval=0.5)
        self.assertEqual(task.callback, callback)
        self.assertEqual(task.frequency, 2)
        self.assertEqual(task.interval, 0.5)
        self.assertEqual(task.last_called, 0.00)

    def test_assert_no_timing(self):
        with self.assertRaises(AssertionError):
            task = Task(callback=mock.Mock())

    def test_assert_both_timing(self):
        with self.assertRaises(AssertionError):
            task = Task(callback=mock.Mock(), frequency=30, interval=0.5)

    def test_call(self):
        callback = mock.Mock()
        task = Task(callback=callback, frequency=30)
        task.call(now=123)
        callback.assert_called_once_with()
        self.assertEqual(task.last_called, 123)


class TestTaskManager(TestCase):

    def test_initialize(self):
        tm = TaskManager()
        self.assertEqual(tm._tasks, [])

    def test_add_task(self):
        tm = TaskManager()
        callback = mock.Mock()
        task = Task(callback=callback, frequency=30)
        t = tm.add_task(task)
        self.assertEqual(t, task)
        self.assertEqual(len(tm._tasks), 1)
        self.assertEqual(tm._tasks[0], task)

    def test_remove_task(self):
        tm = TaskManager()
        callback = mock.Mock()
        task = Task(callback=callback, frequency=30)
        t = tm.add_task(task)
        self.assertEqual(t, task)
        self.assertEqual(len(tm._tasks), 1)
        self.assertEqual(tm._tasks[0], task)
        tm.remove_task(task)
        self.assertEqual(len(tm._tasks), 0)

    def test_remove_task_fails_silently(self):
        tm = TaskManager()
        callback = mock.Mock()
        task = Task(callback=callback, frequency=30)
        t = tm.remove_task(task)  # There is no actual test but we'll see it it throws :P
        self.assertEqual(t, task)

    def test_create_task(self):
        tm = TaskManager()
        callback = mock.Mock()
        with mock.patch.object(tm, 'add_task') as add:
            tm.create_task(callback=callback, frequency=30)
            self.assertEqual(add.call_count, 1)
            self.assertIsInstance(add.call_args[0][0], Task)
            self.assertEqual(add.call_args[0][0].callback, callback)
            self.assertEqual(add.call_args[0][0].frequency, 30)
            self.assertEqual(add.call_args[0][0].interval, 1 / 30)
            self.assertEqual(add.call_args[0][0].last_called, 0.00)

    def test_step_first_call(self):
        tm = TaskManager()
        callback = mock.Mock()
        task = Task(callback=callback, frequency=1)
        t = tm.add_task(task)
        tm.step(1)
        callback.assert_called_once_with()

    def test_step_second_call(self):
        tm = TaskManager()
        callback = mock.Mock()
        task = Task(callback=callback, frequency=1)
        t = tm.add_task(task)
        tm.step(1)
        tm.step(2)
        self.assertEqual(callback.call_count, 2)

    def test_step_frequency(self):
        tm = TaskManager()
        callback = mock.Mock()
        task = Task(callback=callback, frequency=1)
        t = tm.add_task(task)
        tm.step(1)
        tm.step(1.5)
        tm.step(2)
        self.assertEqual(callback.call_count, 2)

    def test_step_interval(self):
        tm = TaskManager()
        callback = mock.Mock()
        task = Task(callback=callback, interval=1)
        t = tm.add_task(task)
        tm.step(1)
        tm.step(1.5)
        tm.step(2)
        self.assertEqual(callback.call_count, 2)