"""
Library for code shared across all SAT's libs
"""

__author__ = "François Ubald Brien"
__copyright__ = "Copyright 2017, Société des arts technologiques"
__credits__ = ["Nicolas Bouillot","François Ubald Brien", "Emmanuel Durand", "Michał Seta", "Jérémie Soria"]
__license__ = "GPLv3"
__version__ = "1.0.0"
__maintainer__ = "François Ubald Brien"
__email__ = "metalab@sat.qc.ca"
__status__ = "Development"
