from abc import ABCMeta, abstractmethod


class Disposable(metaclass=ABCMeta):

    @abstractmethod
    def dispose(self) -> None:
        """
        Dispose of the instance.
        Usually, you should call `super().dispose()` only after having disposed
        of the class' "stuff", in case the disposal depends on "stuff" from the
        parent class.
        :return: None
        """
