import json
import os
from configparser import ConfigParser, SectionProxy
from typing import Optional, Dict, Any, cast


def defaults(defaults: Dict[str, Any], config: Optional[SectionProxy] = None) -> SectionProxy:
    """
    Use a dictionary to set default values on an existing config or a new one if none was passed

    :param defaults: Dict[str, Any]
    :param config: Optional[SectionProxy]
    :return: None
    """
    if not config:
        parser = ConfigParser()
        parser.add_section('default')
        config = parser['default']
    for key, value in defaults.items():
        config.setdefault(key, str(value))
    return config


def load_json_config(file_path: str) -> Dict[str, Any]:
    config = dict()  # type: Dict[str, Any]
    if os.path.exists(file_path):
        with open(file_path, 'r') as config_file:
            config = json.loads(config_file.read())
    return config


def update_json_config(config: Dict[str, Any], file_path: str) -> None:
    config_updates = dict()  # type: Dict[str, Any]
    if os.path.exists(file_path):
        with open(file_path, 'r') as config_file:
            config_updates = json.loads(config_file.read())

    def update_dict(source: Dict[str, Any], update: Dict[str, Any]) -> None:
        for key, value in update.items():
            if key in source:
                if isinstance(update[key], dict):
                    update_dict(source[key], update[key])
                else:
                    source[key] = value
            else:
                source[key] = value

    update_dict(config, config_updates)


def save_json_config(file_path: str, config: Dict[str, Any]) -> None:
    with open(file_path, 'w') as config_file:
        json.dump(config, config_file, indent=2)
