from typing import Any


class Logger:
    indent = 0

    def __init__(self, target: Any) -> None:
        self._name = target.__class__.__name__

    def open(self, *args: Any, **kwargs: Any) -> None:
        color = 31 + (len(self._name) % 6)  # 6 available colors + 1 because of modulo
        self.print('┌', '\033[1;' + str(color) + 'm', True, *args, **kwargs)
        Logger.indent += 1

    def log(self, *args: Any, **kwargs: Any) -> None:
        self.print('', '\033[1;90m', True, *args, **kwargs)

    def print(self, char: str, color: str, show_name: bool, *args: Any, **kwargs: Any) -> None:
        print('\033[1;90m' + ("│   " * Logger.indent) + char + ' ' + ((color + self._name + ':') if show_name else '') + '\033[0m', *args, **kwargs, end='')  # type: ignore
        print('\033[0m')

    def close(self, *args: Any, **kwargs: Any) -> None:
        Logger.indent -= 1
        if Logger.indent < 0:
            Logger.indent = 0
        self.print('└', '\033[1;96m', False, *args, **kwargs)
