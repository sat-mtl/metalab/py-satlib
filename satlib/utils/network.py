from typing import cast
import socket
import struct


def get_default_gateway_linux() -> str:
    """
    Get the IP of the default network interface
    source: https://gist.github.com/ssokolow/1059982
    :return: The IP address of the default interface if found, 'localhost otherwise
    """
    with open("/proc/net/route") as fh:
        for line in fh:
            fields = line.strip().split()
            # The default gateway doesn't have a mask
            if fields[1] != '00000000' or not int(fields[3], 16) & 2:
                continue
            gw_ip = socket.inet_ntoa(struct.pack("<L", int(fields[2], 16)))
            # We connect a dummy socket to the default gateway IP to get our default interface IP address
            with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s:
                try:
                    s.connect((gw_ip, 0))
                except:
                    break
                else:
                    return cast(str, s.getsockname()[0])
    return 'localhost'
