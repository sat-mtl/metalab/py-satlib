from typing import Callable, List, Optional


class Task:

    def __init__(self, callback: Callable, frequency: Optional[float] = None, interval: Optional[float] = None) -> None:
        assert frequency is not None or interval is not None
        assert not (frequency is not None and interval is not None)
        assert frequency is None or frequency > 0

        self.callback = callback
        self.frequency = frequency if frequency is not None else 1.00 / interval if interval is not None else 0.00
        self.interval = interval if interval is not None else 1.00 / frequency if frequency is not None else 0.00
        self.last_called = 0.00

    def call(self, now: float) -> None:
        self.callback()
        self.last_called = now


class TaskManager:
    """
    Crude task manager to manage a list of callbacks to be called on different intervals
    """

    def __init__(self) -> None:
        self._tasks = []  # type: List[Task]

    def add_task(self, task: Task) -> Task:
        self._tasks.append(task)
        return task

    def remove_task(self, task: Task) -> Task:
        try:
            self._tasks.remove(task)
        except ValueError:
            pass
        return task

    def create_task(self, callback: Callable, frequency: Optional[float] = None, interval: Optional[float] = None) -> Task:
        return self.add_task(Task(callback=callback, frequency=frequency, interval=interval))

    def step(self, now: float) -> None:
        for task in self._tasks:
            # print(task, task.last_called, task.interval)
            if now - task.last_called >= task.interval:
                task.call(now=now)
