from typing import TYPE_CHECKING
from logging import Formatter

if TYPE_CHECKING:
    from logging import LogRecord


class SATFormatter(Formatter):
    """
    Nice little colorful logging formatter
    """
    def formatMessage(self, record: 'LogRecord') -> str:
        color = '\033[0;0m'
        if record.levelno == 10:
            color = '\033[0;37m'
        elif record.levelno == 20:
            color = '\033[0;34m'
        elif record.levelno == 30:
            color = '\033[0;33m'
        elif record.levelno == 40:
            color = '\033[0;31m'
        elif record.levelno == 50:
            color = '\033[0;35m'
        # record.levelname + "
        return color + "[\033[1;30m" + record.name + color + "] " + record.getMessage() + "\033[0;0m"
