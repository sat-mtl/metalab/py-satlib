from typing import List, Iterable


def find_unused_name(name: str, used: Iterable[str]) -> str:
    new_name = name
    if new_name in used:
        counter = 0
        found = False
        while not found:
            counter += 1
            new_name = '{}.{0:03d}'.format(name, counter)
            found = new_name not in used
    return new_name
